package astar;

import astar.algorithm.WorldGraphNode;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Double.*;

public class Scores<T extends WorldGraphNode<T>> {
    private final Map<T, Double> gCost = new HashMap<>();
    private final Map<T, Double> hCost = new HashMap<>();

    public void setScores(T node, double gScore, double hScore) {
        this.gCost.put(node, gScore);
        this.hCost.put(node, hScore);
    }

    public double getGCost(final T node) {
        return gCost.getOrDefault(node, MAX_VALUE / 2);
    }

    private double getHCost(final T node) {
        return hCost.getOrDefault(node, MAX_VALUE / 2);
    }

    public void clear() {
        gCost.clear();
        hCost.clear();
    }

    public double getFCost(final T n) {
        return getGCost(n) + getHCost(n);
    }
}
