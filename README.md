# a-star

Implementation of a* routefinding algorithm, which can be extended for different geographic implementations or 
heuristic methods.

See test classes for indication of usage; a default implementation based on 2D space, with a euclidean distance 
heuristic, is included.