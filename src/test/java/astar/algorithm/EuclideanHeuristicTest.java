package astar.algorithm;

import org.junit.jupiter.api.Test;

import java.awt.geom.Point2D;

import static org.assertj.core.api.Assertions.assertThat;

public class EuclideanHeuristicTest {
    @Test
    public void shouldReturnZeroForNodesInSamePosition() {
        final PositionNode2D nodeA = new PositionNode2D("a", new Point2D.Double(0,0));
        final PositionNode2D nodeB = new PositionNode2D("b", new Point2D.Double(0,0));
        final double hScore = new EuclideanHeuristic().function(nodeA, nodeB);
        assertThat(hScore).isEqualTo(0);
    }

    @Test
    public void shouldFindCorrectStraightLineDistanceBetweenNodes() {
        final PositionNode2D nodeA = new PositionNode2D("a", new Point2D.Double(0,0));
        final PositionNode2D nodeB = new PositionNode2D("b", new Point2D.Double(1000,0));
        final double hScore = new EuclideanHeuristic().function(nodeA, nodeB);
        assertThat(hScore).isEqualTo(1000);
    }
}
