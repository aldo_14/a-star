package astar.algorithm;

public abstract class Heuristic<T extends WorldGraphNode<T>> {
    /**
     * Estimate cost from <i>node</i> to <i>goal</i> - i.e. heuristic function
     */
    public abstract double function(final T node, final T goal);
}
