package astar.algorithm;

public abstract class ExactCost<T extends WorldGraphNode<T>> {
    /**
     * Estimate cost from <i>prev</i> to <i>current</i> used to tally g
     *
     * Prev and current must neighbour, or IllegalArgumentException
     */
    public final double function(final T prev, final T current) throws IllegalArgumentException {
        if(!prev.getNeighbours().contains(current)) {
            throw new IllegalArgumentException(prev + " and " + current + " not neighbouring \n" +
                    "(prev neighbours are " + prev.getNeighbours() + ")");
        }
        return cost(prev, current);
    }

    abstract double cost(final T prev, final T current);
}
