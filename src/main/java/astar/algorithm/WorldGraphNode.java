package astar.algorithm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

public abstract class WorldGraphNode<T extends WorldGraphNode<T>> {
    private final String id;
    private final Map<String, T> neighbours;

    protected WorldGraphNode() {
        id = this.getClass().getSimpleName() + ":" + UUID.randomUUID();
        neighbours = new HashMap<>();
    }

    public String getUUID() {
        return id;
    }

    public final void addNeighbours(final T... nodes) {
        Stream.of(nodes)
                .filter(n -> !neighbours.containsKey(n.getUUID()))
                .forEach(n -> {
                    neighbours.put(n.getUUID(), n);
                    n.addNeighbours((T) this);
                });
    }

    public List<T> getNeighbours() {
        return List.copyOf(neighbours.values());
    }
}
