package astar.algorithm;

import java.awt.geom.Point2D;

public class PositionNode2D extends WorldGraphNode<PositionNode2D> {
    private final String name;
    private final Point2D location;

    public PositionNode2D(final String name, final Point2D location) {
        super();
        this.name = name;
        this.location = location;
    }

    public final Point2D getLocation() {
        return location;
    }

    public String getName(){
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
