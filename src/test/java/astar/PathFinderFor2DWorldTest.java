package astar;

import astar.algorithm.Distance2DGoalCost;
import astar.algorithm.EuclideanHeuristic;
import astar.algorithm.PositionNode2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.geom.Point2D;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("SpellCheckingInspection")
public class PathFinderFor2DWorldTest {

    private PathFinder<PositionNode2D> testee;

    @BeforeEach
    private void setup() {
        testee = new PathFinder<>(new Distance2DGoalCost(), new EuclideanHeuristic());
    }

    @Test
    public void shouldReturnAEmptyListWhereNoRouteExists() {
        final PositionNode2D a = new PositionNode2D("a", new Point2D.Double(0, 0));
        final PositionNode2D b = new PositionNode2D("b", new Point2D.Double(10, 0));
        assertThat(testee.findRoute(a, b)).isEmpty();
    }

    @Test
    public void shouldReturnASingleNodeRouteWhereAskingNodeToFindPathToItself() {
        final PositionNode2D a = new PositionNode2D("a", new Point2D.Double(0, 0));
        assertThat(testee.findRoute(a, a)).containsExactly(a);
    }

    @Test
    public void shouldReturnAPathWithStartNodeAtBeginningAndGoalAtEnd() {
        final PositionNode2D a = new PositionNode2D("a", new Point2D.Double(0, 0));
        final PositionNode2D b = new PositionNode2D("b", new Point2D.Double(10, 0));
        a.addNeighbours(b);
        assertThat(testee.findRoute(a, b)).containsExactly(a, b);
    }

    @Test
    public void shouldReturnCorrectPathWithSimpleStraightLinearWorld() {
        final PositionNode2D a = new PositionNode2D("a", new Point2D.Double(0, 0));
        final PositionNode2D b = new PositionNode2D("b", new Point2D.Double(10, 0));
        final PositionNode2D c = new PositionNode2D("c", new Point2D.Double(10, 10));
        a.addNeighbours(b);
        b.addNeighbours(c);
        assertThat(testee.findRoute(a, c)).containsExactly(a, b, c);
    }

    @Test
    public void shouldReturnCorrectPathWithSimpleStraightLinearWorldWhenGoalAtMidway() {
        final PositionNode2D a = new PositionNode2D("a", new Point2D.Double(0, 0));
        final PositionNode2D b = new PositionNode2D("b", new Point2D.Double(10, 0));
        final PositionNode2D c = new PositionNode2D("c", new Point2D.Double(10, 10));
        final PositionNode2D d = new PositionNode2D("d", new Point2D.Double(25, 10));
        a.addNeighbours(b);
        b.addNeighbours(c);
        c.addNeighbours(d);
        assertThat(testee.findRoute(a, c)).containsExactly(a, b, c);
    }


    @Test
    public void shouldReturnShortestPathWithDiamondWorld() {
        final PositionNode2D a = new PositionNode2D("a", new Point2D.Double(0, -20));
        final PositionNode2D b = new PositionNode2D("b", new Point2D.Double(10, 0));
        final PositionNode2D c = new PositionNode2D("c", new Point2D.Double(-20, 0));
        final PositionNode2D d = new PositionNode2D("d", new Point2D.Double(0, 20));
        a.addNeighbours(b, c);
        b.addNeighbours(d);
        c.addNeighbours(d);
        assertThat(testee.findRoute(a, d)).containsExactly(a, b, d);
    }

    @Test
    public void shouldReturnShortestPathWithDiamondWorldWhenBacktrackingRequired() {
        final PositionNode2D start = new PositionNode2D("start", new Point2D.Double(0, -100));
        final PositionNode2D l1 = new PositionNode2D("l1", new Point2D.Double(2, 98));
        final PositionNode2D l2 = new PositionNode2D("l2", new Point2D.Double(1, 99));
        final PositionNode2D l3 = new PositionNode2D("l3", new Point2D.Double(100, 50));
        final PositionNode2D r1 = new PositionNode2D("r1", new Point2D.Double(50, 50));
        final PositionNode2D r2 = new PositionNode2D("r2", new Point2D.Double(25, 90));
        final PositionNode2D end = new PositionNode2D("end", new Point2D.Double(0, 100));
        start.addNeighbours(l1, r1);
        l1.addNeighbours(l2);
        l2.addNeighbours(l3);
        r1.addNeighbours(r2);
        end.addNeighbours(l3, r2);
        assertThat(testee.findRoute(start, end)).containsExactly(start, r1, r2, end);
    }

    @Test
    public void shouldReturnShortestPathWithDiamondWorldWhenShortestPathHasMoreNodesButLessDistance() {
        final PositionNode2D start = new PositionNode2D("start", new Point2D.Double(0, -100));
        final PositionNode2D l1 = new PositionNode2D("l1", new Point2D.Double(2, 98));
        final PositionNode2D l2 = new PositionNode2D("l2", new Point2D.Double(1, 99));
        final PositionNode2D l3 = new PositionNode2D("l3", new Point2D.Double(100, 50));
        final PositionNode2D r1 = new PositionNode2D("r1", new Point2D.Double(50, 50));
        final PositionNode2D r2 = new PositionNode2D("r2", new Point2D.Double(25, 90));
        final PositionNode2D r3 = new PositionNode2D("r3", new Point2D.Double(20, 91));
        final PositionNode2D r4 = new PositionNode2D("r4", new Point2D.Double(15, 92));
        final PositionNode2D r5 = new PositionNode2D("r5", new Point2D.Double(10, 95));
        final PositionNode2D r6 = new PositionNode2D("r6", new Point2D.Double(5, 97));
        final PositionNode2D end = new PositionNode2D("end", new Point2D.Double(0, 100));
        start.addNeighbours(l1, r1);
        l1.addNeighbours(l2);
        l2.addNeighbours(l3);
        r1.addNeighbours(r2);
        r2.addNeighbours(r3);
        r3.addNeighbours(r4);
        r4.addNeighbours(r5);
        r5.addNeighbours(r6);
        end.addNeighbours(l3, r6);
        assertThat(testee.findRoute(start, end)).containsExactly(start, r1, r2, r3, r4, r5, r6, end);
    }

    @Test
    public void shouldReturnShortestPathWithDiamondWorldWhenItInvolvesMoreNodesButLessDistance() {
        final PositionNode2D a = new PositionNode2D("a", new Point2D.Double(0, -20));
        final PositionNode2D b = new PositionNode2D("b", new Point2D.Double(10, 0));
        final PositionNode2D c = new PositionNode2D("c", new Point2D.Double(20, 5));
        final PositionNode2D d = new PositionNode2D("d", new Point2D.Double(-300, 20));
        final PositionNode2D e = new PositionNode2D("e", new Point2D.Double(0, 80));
        a.addNeighbours(b, d);
        b.addNeighbours(c);
        c.addNeighbours(e);
        d.addNeighbours(e);
        assertThat(testee.findRoute(a, e)).containsExactly(a, b, c, e);
    }

    @Test
    public void shouldFindCorrectPathForFullyConnectedGridMapWithNoObstacle() {
        /*
        [aa][ab][ac][ad][ae]
        [ba][bb][bc][bd][be]
        [ca][cb][cc][cd][ce]
        [da][db][dc][dd][de]
         */
        final var aa = new PositionNode2D("aa", new Point2D.Double(0, 0));
        final var ab = new PositionNode2D("ab", new Point2D.Double(10, 0));
        final var ac = new PositionNode2D("ac", new Point2D.Double(20, 0));
        final var ad = new PositionNode2D("ad", new Point2D.Double(30, 0));
        final var ae = new PositionNode2D("ae", new Point2D.Double(40, 0));

        final var ba = new PositionNode2D("ba", new Point2D.Double(0, 10));
        final var bb = new PositionNode2D("bb", new Point2D.Double(10, 10));
        final var bc = new PositionNode2D("bc", new Point2D.Double(20, 10));
        final var bd = new PositionNode2D("bd", new Point2D.Double(30, 10));
        final var be = new PositionNode2D("be", new Point2D.Double(40, 10));

        final var ca = new PositionNode2D("ca", new Point2D.Double(0, 20));
        final var cb = new PositionNode2D("cb", new Point2D.Double(10, 20));
        final var cc = new PositionNode2D("cc", new Point2D.Double(20, 20));
        final var cd = new PositionNode2D("cd", new Point2D.Double(30, 20));
        final var ce = new PositionNode2D("ce", new Point2D.Double(40, 20));

        final var da = new PositionNode2D("da", new Point2D.Double(0, 30));
        final var db = new PositionNode2D("db", new Point2D.Double(10, 30));
        final var dc = new PositionNode2D("dc", new Point2D.Double(20, 30));
        final var dd = new PositionNode2D("dd", new Point2D.Double(30, 30));
        final var de = new PositionNode2D("de", new Point2D.Double(40, 30));

        aa.addNeighbours(ab, ba, bb);
        ab.addNeighbours(aa, ba, bb, ac, bc);
        ac.addNeighbours(ab, bb, bc, ad, bd);
        ad.addNeighbours(ac, bc, db, ae, be);
        ae.addNeighbours(ad, bd, be);

        ba.addNeighbours(aa, ab, bb, ca, cb);
        bb.addNeighbours(aa, ab, ac, ba, bc, ca, cb, cc);
        bc.addNeighbours(ab, ac, ad, bb, bd, cb, cc, cd);
        bd.addNeighbours(ac, ad, ae, bc, be, cc, cd, ce);
        be.addNeighbours(ad, ae, bd, cd, ce);

        ca.addNeighbours(ba, bb, cb, da, db);
        cb.addNeighbours(ba, bb, bc, ca, cc, da, db, dc);
        cc.addNeighbours(bb, bc, bd, cb, cd, db, dc, dd);
        cd.addNeighbours(bc, bd, be, cc, ce, dc, dd, de);
        ce.addNeighbours(bd, be, cd, dd, de);

        da.addNeighbours(ca, cb, db);
        db.addNeighbours(ca, cb, cc, da, dc);
        dc.addNeighbours(cb, cc, cd, db, dd);
        dd.addNeighbours(cc, cd, ce, dc, de);
        de.addNeighbours(cd, ce, dd);

        assertThat(testee.findRoute(aa, ab)).containsExactly(aa, ab);
    }

    @Test
    public void shouldFindCorrectPathForGridMapWithSimpleLineObstacle() {
        /*
        [aa][ab][ac]
        [ba][XX][XX]
        [ca][cb][cc]
         */
        final var aa = new PositionNode2D("aa", new Point2D.Double(0, 0));
        final var ab = new PositionNode2D("ab", new Point2D.Double(10, 0));
        final var ac = new PositionNode2D("ac", new Point2D.Double(20, 0));

        final var ba = new PositionNode2D("ba", new Point2D.Double(0, 10));

        final var ca = new PositionNode2D("ca", new Point2D.Double(0, 20));
        final var cb = new PositionNode2D("cb", new Point2D.Double(10, 20));
        final var cc = new PositionNode2D("cc", new Point2D.Double(20, 20));

        aa.addNeighbours(ab, ba);
        ab.addNeighbours(aa, ac);
        ac.addNeighbours(ab);

        ba.addNeighbours(aa, ca);

        ca.addNeighbours(ba, cb);
        cb.addNeighbours(ca, cc);
        cc.addNeighbours(cb);

        assertThat(testee.findRoute(ac, cc)).containsExactly(ac, ab, aa, ba, ca, cb, cc);
    }

    @Test
    public void shouldFindShorterPathForGridMapWithSimpleLineObstacle() {
        /*
        [aa][ab][ac][ad]
        [ba][XX][bc][XX]
        [ca][cb][cc][cd]
         */
        final var aa = new PositionNode2D("aa", new Point2D.Double(0, 0));
        final var ab = new PositionNode2D("ab", new Point2D.Double(10, 0));
        final var ac = new PositionNode2D("ac", new Point2D.Double(20, 0));
        final var ad = new PositionNode2D("ad", new Point2D.Double(30, 0));

        final var ba = new PositionNode2D("ba", new Point2D.Double(0, 10));
        final var bc = new PositionNode2D("bc", new Point2D.Double(20, 10));

        final var ca = new PositionNode2D("ca", new Point2D.Double(0, 20));
        final var cb = new PositionNode2D("cb", new Point2D.Double(10, 20));
        final var cc = new PositionNode2D("cc", new Point2D.Double(20, 20));
        final var cd = new PositionNode2D("cd", new Point2D.Double(30, 20));

        aa.addNeighbours(ab, ba);
        ab.addNeighbours(aa, ac);
        ac.addNeighbours(ab, ad, bc);
        ad.addNeighbours(ac);
        ba.addNeighbours(aa, ca);
        bc.addNeighbours(ac, cc);
        ca.addNeighbours(ba, cb);
        cb.addNeighbours(ca, cc);
        cc.addNeighbours(bc, cb, cd);
        cd.addNeighbours(cc);

        assertThat(testee.findRoute(ad, cd)).containsExactly(ad, ac, bc, cc, cd);
    }

    @Test
    public void shouldFindCorrectPathForFullyConnectedGridMapWithConcaveObstacle() {
        /*
        [aa][ab][ac][ad][ae]
        [ba][XX][XX][XX][be]
        [ca][cb][cc][XX][ce]
        [da][XX][XX][XX][de]
        [ea][eb][XX][XX][ee]
         */
        final var aa = new PositionNode2D("aa", new Point2D.Double(0, 0));
        final var ab = new PositionNode2D("ab", new Point2D.Double(10, 0));
        final var ac = new PositionNode2D("ac", new Point2D.Double(20, 0));
        final var ad = new PositionNode2D("ad", new Point2D.Double(30, 0));
        final var ae = new PositionNode2D("ae", new Point2D.Double(40, 0));

        final var ba = new PositionNode2D("ba", new Point2D.Double(0, 10));
        final var be = new PositionNode2D("be", new Point2D.Double(40, 10));

        final var ca = new PositionNode2D("ca", new Point2D.Double(0, 20));
        final var cb = new PositionNode2D("cb", new Point2D.Double(10, 20));
        final var cc = new PositionNode2D("cc", new Point2D.Double(20, 20));
        final var ce = new PositionNode2D("ce", new Point2D.Double(40, 20));

        final var da = new PositionNode2D("da", new Point2D.Double(0, 30));
        final var de = new PositionNode2D("de", new Point2D.Double(40, 30));

        final var ea = new PositionNode2D("da", new Point2D.Double(0, 40));
        final var eb = new PositionNode2D("de", new Point2D.Double(10, 40));
        final var ee = new PositionNode2D("de", new Point2D.Double(40, 40));

        aa.addNeighbours(ab, ba);
        ab.addNeighbours(aa, ac);
        ac.addNeighbours(ac, ad);
        ad.addNeighbours(ac, ae);
        ae.addNeighbours(ad, ae);
        ba.addNeighbours(aa, ca);
        be.addNeighbours(ae, ce);
        ca.addNeighbours(ba, da);
        cb.addNeighbours(ca);
        cc.addNeighbours(cb);
        da.addNeighbours(ca, ea);
        de.addNeighbours(ce, ee);
        ea.addNeighbours(da, eb);
        eb.addNeighbours(ea);
        ee.addNeighbours(de);

        assertThat(testee.findRoute(cc, ee)).containsExactly(cc, cb, ca, ba, aa, ab, ac, ad, ae, be, ce, de, ee);
    }
}
