package astar;

import astar.algorithm.ExactCost;
import astar.algorithm.Heuristic;
import astar.algorithm.WorldGraphNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static java.util.Collections.*;
import static java.util.Comparator.comparing;

public class PathFinder<T extends WorldGraphNode<T>> {
    public static final Logger LOGGER = LoggerFactory.getLogger(PathFinder.class);

    private final ExactCost<T> exactCost;
    private final Heuristic<T> heuristic;
    private final LinkedList<T> open;
    private final Scores<T> score;
    private final Map<T, T> bestConnection;

    public PathFinder(final ExactCost<T> exactCost, final Heuristic<T> heuristic) {
        this.exactCost = exactCost;
        this.heuristic = heuristic;
        this.open = new LinkedList<>();
        this.score = new Scores<>();
        this.bestConnection = new HashMap<>();
    }

    public List<T> findRoute(final T start, final T goal) {
        initSets(start, goal);

        while(!open.isEmpty()) {
            var lowestF = open.stream().min(comparing(n -> score.getFCost(n))).get();

            if(lowestF.equals(goal)) {
                var route = buildPath(start, goal);
                LOGGER.debug("Path found " + route);
                return route;
            }

            open.remove(lowestF);
            lowestF.getNeighbours()
                    .forEach(n -> consider(n, lowestF, heuristic.function(n, goal)));
        }

        LOGGER.warn("Unable to find a route from " + start + " to " + goal);
        return emptyList();
    }

    private void initSets(T start, T goal) {
        bestConnection.clear();
        score.clear();
        score.setScores(start, 0, heuristic.function(start, goal));
        open.clear();
        open.add(start);
    }

    private void consider(final T node, final T parent, final double hScore) {
        final var gScore = this.score.getGCost(parent) + exactCost.function(parent, node);
        if(gScore < this.score.getGCost(node)) {
            bestConnection.put(node, parent);
            this.score.setScores(node, gScore, hScore);
            if(!open.contains(node)) {
                open.add(node);
            }
        }
    }

    private List<T> buildPath(final T start, final T goal) {
        final var route = new LinkedList<T>(Collections.singleton(goal));
        var current = goal;

        while(!current.equals(start)) {
            current = bestConnection.get(current);
            route.push(current);
        }

        return route;
    }
}
