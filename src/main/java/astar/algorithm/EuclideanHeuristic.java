package astar.algorithm;

public class EuclideanHeuristic extends Heuristic<PositionNode2D> {
    @Override
    public double function(final PositionNode2D node, final PositionNode2D goal) {
        return node.getLocation().distance(goal.getLocation());
    }
}
