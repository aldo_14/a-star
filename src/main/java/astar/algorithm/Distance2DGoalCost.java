package astar.algorithm;

public class Distance2DGoalCost extends ExactCost<PositionNode2D> {
    @Override
    double cost(final PositionNode2D prev, final PositionNode2D current) {
        return prev.getLocation().distance(current.getLocation());
    }
}
